---
name: 

authors: username

role: 

organizations:
- name: 
  url: ""
- name: 
  url: ""

# Social and academic network
social:
# email
- icon: envelope
  icon_pack: fas
  link: 'mailto:'
# Github  
- icon: github
  icon_pack: fab
  link:
# Twitter  
- icon: twitter
  icon_pack: fab
  link:
# Google scholar  
- icon: google-scholar
  icon_pack: ai
  link:


superuser: false
user_groups: 
- Auteurs
---
